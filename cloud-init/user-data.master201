#cloud-config
autoinstall:
  version: 1

  network:
    version: 2
    ethernets:
      eno1:
        dhcp4: no
      eno2:
        addresses:
          - 192.168.5.127/24
      ens6f0np0:
        dhcp4: no
      ens6f1np1:
        dhcp4: no
    bonds:
      bond0:
        interfaces: ["ens6f0np0","ens6f1np1"]
        addresses:
          - 137.224.210.243/28
      parameters: {mode: 802.3ad, lacp-rate: slow, mii-monitor-interval: 100}
        routes:
          - to: default
            via: 137.224.210.141
        nameservers:
          search: [wur.nl, wurnet.nl]
          addresses: [10.90.3.31, 10.90.7.14, 10.91.3.31]
    vlans:
      vlan.600:
        id: 600
        link: eno1
        accept-ra: no
        addresses:
          - 192.168.16.187/23
      vlan.601:
        id: 601
        link: eno1
        accept-ra: no
        addresses:
          - 192.168.0.127/23

  storage:
    grub:
      reorder_uefi: False
    config:
      - id: disk0
        type: disk
        ptable: gpt
        preserve: false

      - id: disk0-efi
        type: partition
        number: 1
        size: 512MB
        device: disk0
        flag: boot
        grub_device: true
        preserve: false
      - id: disk0-efi-fs
        type: format
        fstype: fat32
        volume: disk0-efi
        preserve: false
      - id: disk0-efi-mount
        type: mount
        path: /boot/efi
        device: disk0-efi-fs
        preserve: false

      - id: disk0-boot
        type: partition
        number: 2
        size: 4GB
        device: disk0
        preserve: false
      - id: disk0-boot-fs
        type: format
        fstype: ext4
        volume: disk0-boot
        preserve: false
      - id: disk0-boot-mount
        type: mount
        path: /boot
        device: disk0-boot-fs
        options: 'defaults,noatime,discard,errors=remount-ro'
        preserve: false

      - id: disk0-pv
        type: partition
        number: 3
        size: -1
        device: disk0
        preserve: false
      - id: Vsys
        type: lvm_volgroup
        name: Vsys
        devices:
          - disk0-pv
        preserve: false

      - id: Vsys-Lroot
        type: lvm_partition
        name: Lroot
        size: 30GB
        volgroup: Vsys
        preserve: false
      - id: Vsys-Lroot-fs
        type: format
        fstype: ext4
        volume: Vsys-Lroot
        preserve: false
      - id: Vsys-Lroot-mount
        type: mount
        path: /
        device: Vsys-Lroot-fs
        options: 'defaults,noatime,discard,errors=remount-ro'
        preserve: false

      - id: Vsys-Llog
        type: lvm_partition
        name: Llog
        size: 20GB
        volgroup: Vsys
        preserve: false
      - id: Vsys-Llog-fs
        type: format
        fstype: ext4
        volume: Vsys-Llog
        preserve: false
      - id: Vsys-Llog-mount
        type: mount
        path: /var/log
        device: Vsys-Llog-fs
        options: 'defaults,noatime,discard,errors=remount-ro'
        preserve: false

      - id: Vsys-Lopt
        type: lvm_partition
        name: Lopt
        size: 30GB
        volgroup: Vsys
        preserve: false
      - id: Vsys-Lopt-fs
        type: format
        fstype: ext4
        volume: Vsys-Lopt
        preserve: false
      - id: Vsys-Lopt-mount
        type: mount
        path: /opt
        device: Vsys-Lopt-fs
        options: 'defaults,noatime,discard,errors=remount-ro'
        preserve: false

      - id: Vsys-Linfluxdb
        type: lvm_partition
        name: Linfluxdb
        size: 50GB
        volgroup: Vsys
        preserve: false
      - id: Vsys-Linfluxdb-fs
        type: format
        fstype: ext4
        volume: Vsys-Linfluxdb
        preserve: false
      - id: Vsys-Linfluxdb-mount
        type: mount
        path: /var/lib/influxdb
        device: Vsys-Linfluxdb-fs
        options: 'defaults,noatime,discard,errors=remount-ro'
        preserve: false

      - id: Vsys-Lmysql
        type: lvm_partition
        name: Lmysql
        size: 150GB
        volgroup: Vsys
        preserve: false
      - id: Vsys-Lmysql-fs
        type: format
        fstype: ext4
        volume: Vsys-Lmysql
        preserve: false
      - id: Vsys-Lmysql-mount
        type: mount
        path: /var/lib/mysql
        device: Vsys-Lmysql-fs
        options: 'defaults,noatime,discard,errors=remount-ro'
        preserve: false

  user-data: 
    hostname: master201.anunna.wur.nl
    disable_root: false
    runcmd:
      - update-grub
    users: []

#  packages:
#     - apt-transport-https
#     - apt-utils
##     - ansible
#     - curl
##     - dos2unix
#     - git
#     - gpg
#     - jnettop
#     - joe
#     - keyutils
#     - krb5-user
#     - ldap-utils
#     - ldapscripts
#     - libpam-krb5
#     - libpam-mount
#     - libssl-dev
#     - lvm2
#     - mailutils
#     - mc
#     - nano
#     - net-tools
#     - ntfs-3g
#     - nscd
#     - nslcd
#     - ntp
#     - ntpdate
#     - open-vm-tools
#     - openssh-server
#     - pv
#     - pwgen
#     - python3
#     - python-is-python3
#     - python3-pip
#     - samba
#     - screen
#     - smbclient
#     - ssl-cert
#     - sssd
#     - sssd-ad
#     - sssd-common
#     - sssd-krb5
#     - sssd-ldap
#     - samba-common
#     - samba-common-bin
#     - telnet
#     - tree
#     - unzip
#     - vim
#     - wget
#     - xxd
#     - zip
##HPC specific
#     - apache2
#     - bind9
#     - build-essential
#     - cifs-utils
#     - corosync
#     - debootstrap
#     - fakeroot
#     - freeipmi
#     - nfs-common
#     - nfs-kernel-server
#     - open-iscsi
#     - pcs
#     - pxelinux
#     - quilt
#     - rsync
#     - slapd
#     - syslinux

  late-commands:
    - sed -i 's|^root:[^:]*:|root:$ROOT_CRYPT:|' /target/etc/shadow
    - git clone --single-branch https://git.wur.nl/anunna/ishtar /target/root/ishtar
#    - echo '[Unit]\nDescription=Finish HPC rollout\nAfter=network.target\n\n[Service]\nExecStart=/bin/sh -c "cd /root/sali-custom/ansible && ansible-playbook --connection=local -i127.0.0.1, -l127.0.0.1 single-install.yml"\n\nExecStart=-/bin/rm /etc/systemd/system/multi-user.target.wants/rollout-finish.service\nExecStart=-/bin/rm /etc/systemd/system/rollout-finish.service\nExecStart=-/bin/rm /root/ansible-rollout.sh\nExecStart=-/bin/systemctl daemon-reload\n\nType=oneshot\nRemainAfterExit=yes\n\n[Install]\nWantedBy=multi-user.target' > /target/etc/systemd/system/rollout-finish.service
#    - ln -s /etc/systemd/system/rollout-finish.service /target/etc/systemd/system/multi-user.target.wants/rollout-finish.service

  timezone: Europe/Amsterdam

  power_state:
    delay: "now"
    mode: reboot
    message: Install completed. Rebooting now.
    timeout: 30
    condition: True
