Automated installer for HPC components
--------------------------------------


This code will build you an iso (based on covertsh's user-data
embedder) that you can use to repeatably install HPC master nodes.

IMPORTANT CAVEATS:
------------------

 * DO NOT install when a shared filesystem is accessible! The installer
cannot discern between local and DAS disks.

* This installer throws if it sees LVs of the same name as it's trying to
make. If reinstalling, you'll need to remove those LVs first.
