#!/bin/bash
#Who makes a repo without setting the script +x?
cd $(dirname $0)
if [ "x$1" != "x" ] ; then
  SUFFIX='.'"$1"
  if ! [ -e "cloud-init/user-data${SUFFIX}" ] ; then
    echo "${1} - specific file doesn't exist."
    exit 200
  fi
  DESTINATION="20.04.$1.iso"
else
  SUFFIX=''
  DESTINATION="20.04.custom.iso"
fi
if [ -f secrets ] ; then
  . secrets
  cp "cloud-init/user-data${SUFFIX}" user-data
  sed -i "s|\$PMP_KEY|$PMP_KEY|g" user-data
  sed -i "s|\$ROOT_CRYPT|$ROOT_CRYPT|g" user-data
  sed -i "s|\$DEPLOY_KEY|$DEPLOY_KEY|g" user-data
else
  echo "Please create your secrets file."
  echo "Use the secrets.example file as a template, and fill in the associated elements."
fi
bash ubuntu-autoinstall-generator/ubuntu-autoinstall-generator.sh -r -c -a -u user-data -d "$DESTINATION"
